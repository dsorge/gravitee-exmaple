## 
Copied docker-compose.yaml from 
```
curl https://raw.githubusercontent.com/gravitee-io/gravitee-docker/master/apim/3.x/docker-compose.yml -O  
```

##
Login to console: localhost:8084
```
admin/admin
```

##
Create docker image for example API
```
cd test-api-project
mvn spring-boot:build-image
```

package de.dsorge.graviteeexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GraviteeExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(GraviteeExampleApplication.class, args);
	}

}

package de.dsorge.graviteeexample;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Builder
@Data
@Getter
@Setter
public class StatusResponse {

    private String status;
}

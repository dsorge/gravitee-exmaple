package de.dsorge.graviteeexample;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class ApiController {

    @GetMapping("/test")
    public StatusResponse test(){
        return StatusResponse.builder().status("okay").build();
    }
}
